# Curso Shell Prático - Desafio 3: script myfetch

Solução do desafio 3 do Curso Shell Prático 2022: descobrir como o `neofetch` obtém as informações abaixo para criar um script que produza a seguinte saída:

```
Login        : blau@debxp 
OS           : Debian GNU/Linux-libre (sid) x86_64 
Host         : Vostro 270s 
Kernel       : 5.16.12-gnu 
Uptime       : 7 days, 2 hours, 25 mins 
Packages     : 2072 (dpkg) 
Shell        : bash 5.1.16 
Resolution   : 1920x1080, 1920x1080 
WM           : i3 
Theme        : Arc-Dark [GTK2/3] 
Icons        : debxp-paper-folders [GTK2/3] 
Terminal     : xfce4-terminal 
Terminal Font: Inconsolata 13 
CPU          : Intel i5-3330 (4) @ 3.200GHz 
GPU          : Intel HD Graphics 
Memory       : 3084MiB / 15882MiB 
```

## 1 - Login

Resultado: `usuário@host`

```
get_title() {
    user=${USER:-$(id -un || printf %s "${HOME/*\/}")}

    case $title_fqdn in
        on) hostname=$(hostname -f) ;;
        *)  hostname=${HOSTNAME:-$(hostname)} ;;
    esac

    title=${title_color}${bold}${user}${at_color}@${title_color}${bold}${hostname}
    length=$((${#user} + ${#hostname} + 1))
}
```

### Nome de login do usuário

Técnicas no script:

```
:~$ echo $USER
blau

:~$ id -un
blau

:~$ printf %s "${HOME/*\/}"
blau
```

Esta última, também pode ser expandida com...

```
:~$ printf %s "${HOME##*/}"
blau
```

### Hostname

Técnicas no script:

```
:~$ hostname -f
debxp

:~$ hostname
debxp

:~$ echo $HOSTNAME
debxp
```

> **Nota:** A opção `-f` exibiria um hostname com o nome do domínio (FQDN).

### Minha escolha para o script

```
mf_title=$USER@$HOSTNAME
```

## 2 - Sistema Operacional

Resultado: 

```
OS: Debian GNU/Linux bookworm/sid x86_64
```

Aqui, o `neofetch` testa diversos métodos de acordo com a distribuição/sistema. Como eu uso o Debian GNU/Linux, o que me interessa é este trecho da função `get_distro`:

```
	# Source the os-release file
    for file in /etc/lsb-release /usr/lib/os-release \
		/etc/os-release  /etc/openwrt_release; do
		source "$file" && break
	done
```

### Nome estilizado

O método utilizado aqui é o `source` das variáveis em `/etc/os-release` para obter a string em `PRETTY_NAME`:

```
:~$ source /etc/os-release
:~$ echo $PRETTY_NAME
Debian GNU/Linux bookworm/sid
```

Em vez disso, eu vou extrair a string do arquivo utilizando o `sed`:

```
:~$ sed -nE 's/^PRETTY.*"(.*)"/\1/p' /etc/os-release
Debian GNU/Linux bookworm/sid
```

### Arquitetura

A arquitetura é obtida com outras duas informações sobre o kernel com o utilitário `uname`:

```
    IFS=" " read -ra uname <<< "$(uname -srm)"

    kernel_name="${uname[0]}"
    kernel_version="${uname[1]}"
    kernel_machine="${uname[2]}"
```

Onde, no meu caso:

```
:~$ uname -rms
Linux 5.16.12-gnu x86_64
  ↑      ↑          ↑
 NOME  VERSÃO  ARQUITETURA
```

É conveniente não executar o `uname` todas as vezes que eu quiser uma dessas informações, então, vou aproveitar para obtê-las com a seguinte técnica:

```
:~$ read kname kver karch <<< $(uname -rms)
:~$ echo $kname
Linux
:~$ echo $kver
5.16.12-gnu
:~$ echo $karch
x86_64
```

No script:

```
# Kernel info...
read kname kver karch <<< $(uname -rms)

# OS
my_os="$(sed -nE 's/^PRETTY.*"(.*)"/\1/p' /etc/os-release) $karch"
```

## 3 - Modelo da máquina host

Resultado: `Host: Vostro 270s`

O que me interessa é esse trecho da função `get_model`, onde são testados vários arquivos que poderão conter o modelo do meu hardware:

```
get_model() {
    case $os in
        Linux)
            if [[ -d /system/app/ && -d /system/priv-app ]]; then
                model="$(getprop ro.product.brand) $(getprop ro.product.model)"

            elif [[ -f /sys/devices/virtual/dmi/id/board_vendor ||
                    -f /sys/devices/virtual/dmi/id/board_name ]]; then
                model=$(< /sys/devices/virtual/dmi/id/board_vendor)
                model+=" $(< /sys/devices/virtual/dmi/id/board_name)"

            elif [[ -f /sys/devices/virtual/dmi/id/product_name ||
                    -f /sys/devices/virtual/dmi/id/product_version ]]; then
                model=$(< /sys/devices/virtual/dmi/id/product_name)
                model+=" $(< /sys/devices/virtual/dmi/id/product_version)"

            elif [[ -f /sys/firmware/devicetree/base/model ]]; then
                model=$(< /sys/firmware/devicetree/base/model)

            elif [[ -f /tmp/sysinfo/model ]]; then
                model=$(< /tmp/sysinfo/model)
            fi
        ;;

```

Na minha instalação do Debian, eu só tenho esses arquivos:

```
:~$ cat /sys/devices/virtual/dmi/id/board_vendor 
Dell Inc.

:~$ cat /sys/devices/virtual/dmi/id/board_name
0478VN

:~$ cat /sys/devices/virtual/dmi/id/product_name
Vostro 270s

:~$ cat /sys/devices/virtual/dmi/id/product_version
[VAZIO]
```

No meu script, eu vou unir as três informações:

```
# Host model...
my_host=$(echo $(< /sys/devices/virtual/dmi/id/board_vendor) \
               $(< /sys/devices/virtual/dmi/id/product_name) \
               MB:$(< /sys/devices/virtual/dmi/id/board_name))
```

## 4 - Versão do kernel

Resultado: `Kernel: 5.16.12-gnu`

Informação já obtida junto com a arquitetura. No meu script:

```
# Kernel info...
read kname kver karch <<< $(uname -rms)
            ↑
```

## 5 - Uptime

Resultado: `Uptime: 7 days, 4 hours, 3 mins`

Os trechos da função `get_uptime` que me interessam, são:


```
get_uptime() {
    # Get uptime in seconds.
    case $os in
        Linux|Windows|MINIX)
            if [[ -r /proc/uptime ]]; then
                s=$(< /proc/uptime)
                s=${s/.*}
            else
                boot=$(date -d"$(uptime -s)" +%s)
                now=$(date +%s)
                s=$((now - boot))
            fi
        ;;
		
	    ...

	    d="$((s / 60 / 60 / 24)) days"
        h="$((s / 60 / 60 % 24)) hours"
        m="$((s / 60 % 60)) minutes"

	    # Remove plural if < 2.
        ((${d/ *} == 1)) && d=${d/s}
        ((${h/ *} == 1)) && h=${h/s}
        ((${m/ *} == 1)) && m=${m/s}

        # Hide empty fields.
        ((${d/ *} == 0)) && unset d
        ((${h/ *} == 0)) && unset h
        ((${m/ *} == 0)) && unset m

        uptime=${d:+$d, }${h:+$h, }$m
        uptime=${uptime%', '}
        uptime=${uptime:-$s seconds}
		
		...
```

### Uptime em segundos

O `neofetch` utiliza dois métodos para obter o uptime em segundas:

```
# Método 1:

:~$ cat /proc/uptime 
621193.91 2181706.35
:~$ s=$(cat /proc/uptime); echo ${s/.*}
621195

# Método 2:

:~$ date -d"$(uptime -s)" +'%s'
1646570459
:~$ boot=$(date -d"$(uptime -s)" +'%s')
:~$ date +'%s'
1647191852
:~$ now=$(date +'%s')
:~$ echo $((now - boot))
621399
```

Como eu tenho a opção de utilizar o arquivo `/proc/uptime`, acredito que este seja o método mais eficiente:

```
: "$(< /proc/uptime)"; s=${_/.*}
```

### Conversão para dias, horas e minutos

Método do `neofetch`:

```
d="$((s / 60 / 60 / 24)) days"
h="$((s / 60 / 60 % 24)) hours"
m="$((s / 60 % 60)) minutes"
```

Em seguida, são utilizados vários métodos para o tratamento de plurais, mas isso pode ser feito assim:

```
:~$ d=$((s / 60 / 60 / 24))
:~$ (($d)) && { printf "%d dia" $d; ((d > 1)) && echo 's'; }
7 dias

:~$ h=$((s / 60 / 60 % 24))
:~$ (($h)) && { printf "%d hora" $h; ((h > 1)) && echo 's'; }
4 horas

:~$ m=$((s / 60 % 60))
:~$ (($m)) && { printf "%d minuto" $m; ((m > 1)) && echo 's'; }
47 minutos
```

O que me leva a criar a função `conv_time()`:

```
conv_time () 
{ 
    if (($1)); then
        printf "%d $2" $1
        (($1 > 1)) && echo 's'
    fi
}
```

Uso:

```
:~$ conv_time $((s / 60 / 60 / 24)) dia
7 dias
:~$ conv_time $((s / 60 / 60 % 24)) hora
4 horas
:~$ conv_time $((s / 60 % 60)) minuto
47 minutos

# Caso o resultado seja '0'...

:~$ conv_time $((0)) minuto
:~$
```

### Formatação da saída no meu script

Utilizando a minha função:

```
# Uptime
: "$(< /proc/uptime)"
s=${_/.*}
my_uptime=$(echo $(conv_time $((s / 60 / 60 / 24)) dia) \
                 $(conv_time $((s / 60 / 60 % 24)) hora) \
                 $(conv_time $((s / 60 % 60)) minuto))
```

## 6 - Pacotes instalados

Resultado: `Packages: 2072 (dpkg)`

### Descoberta do gerenciador de pacotes

A primeira etapa da descoberta do número de pacotes instalados, é realizada por uma função que verifica, em uma lista de possíveis gerenciadores de pacotes, aquele que é utilizado no sistema. Se encontrado um gerenciador, ele é armazenado na variável `manager`.

```
has() { type -p "$1" >/dev/null && manager=$1; }
```

No meu caso, o gerenciador é o `dpkg`:

```
:~$ has() { type -p "$1" >/dev/null && manager=$1; }
:~$ has dpkg
:~$ echo $manager
dpkg
```

### Número de pacotes

Definido o gerenciador, é aplicado o método adequado à descoberta do número de pacotes instalados. No meu caso:

```
dpkg-query -f '.\n' -W
```

O `neofetch` utiliza uma função com um loop para contar as linhas resultantes do comando acima, mas basta fazer um *pipe* para o utilitário `wc` para obter o mesmo resultado:

```
:~$ dpkg-query -f '.\n' -W | wc -l
2072
```

No meu script, sabendo qual é o meu gerenciador de pacotes, ficou assim:

```
my_pkgs=$(dpkg-query -f '.\n' -W | wc -l)
```

## 7 - Shell

Resultado: `Shell: bash 5.1.16`

O nome do shell e a sua versão serão obtidos pela função `get_shell`, onde o problema mais relevante é como obter a versão, visto que cada shell tem seus próprios meios de fornecer essa informação. Como eu não tenho outro shell além do bash para testar, vou adotar o seguinte método no meu script:

```
shell_name=$(ps -o 'command' $PPID | tail -1)
my_shell=$([ $shell_name = 'bash' ] && echo "Bash $BASH_VERSION" || echo $shell_name)
```

## 8 - Resolução dos monitores

Resultado: `Resolution: 1920x1080, 1920x1080`

Se o sistema for diferente dos especificados na função `get_resolution` (como é o caso do GNU/Linux), o neofetch utilizará o seguinte método para obter a resolução:

```
resolution="$(xrandr --nograb --current |\
	awk -F 'connected |\\+|\\(' \
	'/ connected.*[0-9]+x[0-9]+\+/ && $2 {printf $2 ", "}')"
```

### A saída do Xrandr

```
:~$ xrandr --nograb --current
Screen 0: minimum 320 x 200, current 3840 x 1080, maximum 16384 x 16384
VGA-1 connected primary 1920x1080+0+0 (normal left inverted right x axis y axis) 476mm x 268mm
   1920x1080     60.00*+
   1680x1050     59.95  
   1280x1024     60.02  
   1280x960      60.00  
   1024x768      60.00  
   800x600       60.32  
   640x480       59.94  
   720x400       70.08  
HDMI-1 connected 1920x1080+1920+0 (normal left inverted right x axis y axis) 160mm x 90mm
   1920x1080     60.00*+  50.00    59.94    30.00    25.00    24.00    29.97    23.98  
   1920x1080i    60.00    50.00    59.94  
   1280x1024     60.02  
   1360x768      60.02  
   1280x720      60.00    50.00    59.94  
   1024x768      60.00  
   800x600       60.32  
   720x576       50.00  
   720x576i      50.00  
   720x480       60.00    59.94  
   640x480       60.00    59.94  
   720x400       70.08  
DP-1 disconnected (normal left inverted right x axis y axis)
```

### Captura das informações

O `neofetch` utiliza uma regex com awk, mas eu vou utilizar o `sed` no meu script:

```
# Resolution
my_res=$(echo $(xrandr --nograb --current | \
sed -nE 's/^([0-9A-Z-]+) connected.* ([0-9]+x[0-9]+).*/\1:\2/p'))
```

O que, no meu caso, resulta em:

```
VGA-1:1920x1080 HDMI-1:1920x1080
```

## 9 - Gerenciador de janelas

Resultado: `WM: i3`

Descartando a verificação de gerenciadores de janelas rodando no Wayland, que eu não posso testar, a informação é obtida pelo utilitário `xprop` na função `get_wm`:

```
[[ -z $wm ]] && type -p xprop &>/dev/null && {
	id=$(xprop -root -notype _NET_SUPPORTING_WM_CHECK)
	id=${id##* }
	wm=$(xprop -id "$id" -notype -len 100 -f _NET_WM_NAME 8t)
	wm=${wm/*WM_NAME = }
	wm=${wm/\"}
	wm=${wm/\"*}
}
```

O que me parece a forma mais genérica e portável (no GNU/linux) para a descoberta de um gerenciador de janelas rodando no Xorg. Sendo assim, este foi o método utilizado no meu script:

```
# Gerenciador de janelas
if command -v xprop &> /dev/null; then
    : "$(xprop -root -notype _NET_SUPPORTING_WM_CHECK)"
    id=${_##* }
    my_wm=$(xprop -id $id -notype -len 100 -f _NET_WM_NAME 8t |\
	    awk -F'"' '/_NET_WM_NAME/{print $2}')
fi
```


## 10 - Tema GTK

Resultado: `Theme: Arc-Dark [GTK2/3]`

Esta informação depende fortemente do ambiente gráfico ou do gerenciador de janelas instalado, o que implica em uma complexidade desnecessária para o nosso exercício. Sendo assim, e tendo em conta que eu só utilizo programas GTK, eu vou simplesmente recorrer ao arquivo de configuração do GTK-3.0:

```
:~$ cat ~/.config/gtk-3.0/settings.ini 
[Settings]
gtk-theme-name=Arc-Dark
gtk-icon-theme-name=debxp-paper-folders
gtk-font-name=Open Sans 10
gtk-cursor-theme-name=breeze_cursors
gtk-cursor-theme-size=0
gtk-toolbar-style=GTK_TOOLBAR_BOTH
gtk-toolbar-icon-size=GTK_ICON_SIZE_LARGE_TOOLBAR
gtk-button-images=1
gtk-menu-images=1
gtk-enable-event-sounds=1
gtk-enable-input-feedback-sounds=1
gtk-xft-antialias=1
gtk-xft-hinting=1
gtk-xft-hintstyle=hintfull
gtk-xft-rgba=rgb
gtk-decoration-layout=appmenu
```

Daqui, eu posso extrair duas informações: o tema GTK e o tema de ícones:

```
gtk_settings=$HOME/.config/gtk-3.0/settings.ini
my_theme=$(grep -Po '^gtk-theme-name=\K.*' $gtk_settings)
my_icons=$(grep -Po '^gtk-icon-theme-name=\K.*' $gtk_settings)
```

## 11 - Tema de ícones

Resultado: `Icons: debxp-paper-folders [GTK2/3] `

Já resolvido no tópico anterior.

## 12 - Terminal em uso

Resultado: `Terminal: xfce4-terminal`

Essa informação é obtida pela linha abaixo da função `get_term`, que busca o caminho do executável correspondente ao processo pai da sessão do shell:

```
[[ $os == Linux ]] && term=$(realpath "/proc/$parent/exe")
```

Onde:

```
:~$ ls -l /proc/$PPID/exe
lrwxrwxrwx 1 blau blau 0 mar 13 12:09 /proc/256741/exe -> /usr/bin/xfce4-terminal
```

Daí o uso do `realpath` (GNU coreutils):

```
:~$ realpath /proc/$PPID/exe
/usr/bin/xfce4-terminal
```

O problema é que, do ponto de vista do meu script, o processo pai não é o terminal, mas o shell em que ele for executado. Para isso, o `neofetch` utiliza a função `get_ppid`, que se resume a isso:

```
ppid="$(grep -i -F "PPid:" "/proc/${1:-$PPID}/status")"
ppid="$(trim "${ppid/PPid:}")"
```

Aqui, é utilizado o arquivo `/proc/PROCESSO_DO_SHELL/status`, onde é buscado o campo `PPid`, este sim o processo pai do processo do shell em que o script foi executado.

Portanto, no meu script:

```
# Terminal
: $(grep -i 'ppid' /proc/$PPID/status)
: $(realpath /proc/$_/exe)
my_term=${_##*/}
```

## 13 - Fonte do terminal

Resultado: `Terminal Font: Inconsolata 13`

Mais uma vez, a informação depende do terminal em uso e não há como implementar um método genérico para isso. Portanto, como eu utilizo o Xfce4-Terminal, vou me preocupar apenas com ele:

```
# Fonte do terminal
my_term_font=$(grep -Poi 'fontname=\K.*' $HOME/.config/xfce4/terminal/terminalrc)
```

## 14 - CPU

Resultado: `CPU: Intel i5-3330 (4) @ 3.200GHz `

A informação é obtida, essencialmente, através da leitura dos campos de `/proc/cpuinfo`:

```
get_cpu() {
    case $os in
        "Linux" | "MINIX" | "Windows")
            # Get CPU name.
            cpu_file="/proc/cpuinfo"

            case $kernel_machine in
                "frv" | "hppa" | "m68k" | "openrisc" | "or"* | "powerpc" | "ppc"* | "sparc"*)
                    cpu="$(awk -F':' '/^cpu\t|^CPU/ {printf $2; exit}' "$cpu_file")"
                ;;

                "s390"*)
                    cpu="$(awk -F'=' '/machine/ {print $4; exit}' "$cpu_file")"
                ;;

                "ia64" | "m32r")
                    cpu="$(awk -F':' '/model/ {print $2; exit}' "$cpu_file")"
                    [[ -z "$cpu" ]] && cpu="$(awk -F':' '/family/ {printf $2; exit}' "$cpu_file")"
                ;;

                *)
                    cpu="$(awk -F '\\s*: | @' \
                            '/model name|Hardware|Processor|^cpu model|chip type|^cpu type/ {
                            cpu=$2; if ($1 == "Hardware") exit } END { print cpu }' "$cpu_file")"
                ;;
            esac
			...
```

Sendo assim, eu preferi a seguinte abordagem no meu script:

```
# CPU
my_cores=$(grep -Po 'cpu cores.*: \K.*' /proc/cpuinfo | uniq)
: "$(grep -Po 'model name.*: \K.*' /proc/cpuinfo | uniq)"
: "$(sed -E 's/\([^)]+\)//g' <<< $_)"
my_cpu="$_ ($my_cores)"
```

Que resulta em:

```
Intel Core i5-3330 CPU @ 3.00GHz (4)
```

Mais uma vez, a captura dessas informações é complexa e dependente demais do hardware para a implementação de uma solução genérica no nosso exercício. Então, já que o `neofetch` também começa listando os dispositivos com `lspci`, eu vou fazer o mesmo, mas de forma limitada ao meu hardware:

```
:~$ lspci | grep -Poi '(vga|3d|display).*\KIntel.*Gen(?=.*)'
Intel Corporation Xeon E3-1200 v2/3rd Gen
```

No meu script:

```
my_gpu=$(lspci | grep -Poi '(vga|3d|display).*\KIntel.*Gen(?=.*)')
```

## 15 - Memória

Resultado: `Memory: 3131MiB / 15882MiB`

O `neofetch` trata as informações no arquivo `/proc/meminfo` com a função `get_memory`:

```
get_memory() {
    case $os in
        "Linux" | "Windows")
            # MemUsed = Memtotal + Shmem - MemFree - Buffers - Cached - SReclaimable
            # Source: https://github.com/KittyKatt/screenFetch/issues/386#issuecomment-249312716
            while IFS=":" read -r a b; do
                case $a in
                    "MemTotal") ((mem_used+=${b/kB})); mem_total="${b/kB}" ;;
                    "Shmem") ((mem_used+=${b/kB}))  ;;
                    "MemFree" | "Buffers" | "Cached" | "SReclaimable")
                        mem_used="$((mem_used-=${b/kB}))"
                    ;;

                    # Available since Linux 3.14rc (34e431b0ae398fc54ea69ff85ec700722c9da773).
                    # If detected this will be used over the above calculation for mem_used.
                    "MemAvailable")
                        mem_avail=${b/kB}
                    ;;
                esac
            done < /proc/meminfo

            if [[ $mem_avail ]]; then
                mem_used=$(((mem_total - mem_avail) / 1024))
            else
                mem_used="$((mem_used / 1024))"
            fi

            mem_total="$((mem_total / 1024))"
        ;;
		...
```

Para efeito do nosso exercício, bastam as informações de memória utilizada e memória total, o que requer algum cálculo. No `neofetch`, os valores são calculados em um loop `while`:

```
while IFS=":" read -r a b; do
	case $a in
		"MemTotal") ((mem_used+=${b/kB})); mem_total="${b/kB}" ;;
		"Shmem") ((mem_used+=${b/kB}))  ;;
		"MemFree" | "Buffers" | "Cached" | "SReclaimable") 	mem_used="$((mem_used-=${b/kB}))" ;;

	    # Available since Linux 3.14rc (34e431b0ae398fc54ea69ff85ec700722c9da773).
        # If detected this will be used over the above calculation for mem_used.
        "MemAvailable") mem_avail=${b/kB} ;;
	esac
done < /proc/meminfo
```

O que eu simplifiquei desta forma no meu script:

```
# Memória
while read f d u; do
    case ${f::-1} in
	MemTotal) mt=$d     ;;
	MemAvailable) md=$d ;;
    esac
done < /proc/meminfo

my_mem="$(( (mt-md) / 1024 ))MiB / $((mt / 1024))MiB"

```

## Resultado

```
:~$ ./myfetch 
Login        : blau@debxp
OS           : Debian GNU/Linux-libre (sid) x86_64
Host         : Dell Inc. Vostro 270s MB:0478VN
Kernel       : 5.16.12-gnu
Uptime       : 7 dias 9 horas 16 minutos
Packages     : 2072 (dpkg)
Shell        : Bash 5.1.16(1)-release
Resolution   : VGA-1:1920x1080 HDMI-1:1920x1080
WM           : i3
Theme        : Arc-Dark
Icons        : debxp-paper-folders
Terminal     : xfce4-terminal
Terminal Font: Inconsolata 13
CPU          : Intel Core i5-3330 CPU @ 3.00GHz (4)
GPU          : Intel Corporation Xeon E3-1200 v2/3rd Gen
Memory       : 3504MiB / 15882MiB
```
